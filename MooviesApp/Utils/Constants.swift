//
//  Constants.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 10/02/22.
//

import Foundation
import UIKit

let API_KEY = "bb08c901ddd0212fe29f2310ea277f82"
let BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original"
let BASE_API_URL = "https://api.themoviedb.org/3/"
let BASE_GENRES_URL = "https://api.themoviedb.org/3/genre/"
let BASE_SEARCH_URL = "https://api.themoviedb.org/3/search/movie?api_key=bb08c901ddd0212fe29f2310ea277f82&query=kimi&page=1"

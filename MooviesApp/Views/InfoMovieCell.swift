//
//  InfoMovieCell.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 10/02/22.
//

import UIKit

class InfoMovieCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgMovie: UIImageView!
    @IBOutlet var principalView: UIView!
    var isMovie = true
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setCornersView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCornersView() {
        self.principalView.layer.cornerRadius = 15
        self.imgMovie.layer.cornerRadius = 15
        self.imgMovie.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func setInfoCell(data: NSDictionary) {
        lblTitle.text = (data.value(forKey: isMovie ? "title": "name") as! String)
        lblDescription.text = (data.value(forKey: isMovie ? "release_date": "first_air_date") as! String)
        let urlImage = String(format: "%@%@", BASE_IMAGE_URL,(data.value(forKey: "poster_path") as! String))
        getImage(poster: urlImage)
    }
    
    func getImage(poster: String) {
        DispatchQueue.global(qos: .background).async {
            let imgUrl = NSURL( string: poster)
            let imageData = NSData(contentsOf: imgUrl! as URL)
            DispatchQueue.main.async {
                self.imgMovie.image = UIImage.init(data: imageData! as Data)
            }
        }
    }
    
}

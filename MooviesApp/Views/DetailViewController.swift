//
//  DetailViewController.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 10/02/22.
//

import UIKit

class DetailViewController: UIViewController {
    
    var dataMovie: NSDictionary!
    var delegateMovie: DelegateMovie!
    var allGenres: NSArray!
    var isMovie = true
    
    @IBOutlet var imgMovie: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblGenres: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        delegateMovie.myDetail = self
        self.delegateMovie.invokeGenres(type: isMovie ? "movie": "tv")
        configureScreenText()
        self.imgMovie.layer.cornerRadius = 10
    }
    
    func configureScreenText() {
        self.lblTitle.text = (dataMovie.value(forKey: isMovie ? "title": "name") as! String)
        self.lblDescription.text = (dataMovie.value(forKey: "overview") as! String)        
        let imgUrl = String(format: "%@%@", BASE_IMAGE_URL, (dataMovie.value(forKey: "backdrop_path") as! String))
        getImage(poster: imgUrl)
    }
    
    func getImage(poster: String) {
        DispatchQueue.global(qos: .background).async {
            let imgUrl = NSURL( string: poster)
            let imageData = NSData(contentsOf: imgUrl! as URL)
            DispatchQueue.main.async {
                self.imgMovie.image = UIImage.init(data: imageData! as Data)
            }
        }
    }
    
    func receiveGenres(genres: NSArray) {
        self.allGenres = genres
        searchGenres()
    }
    
    func searchGenres() {
        var genresText = ""
        
        for currentGenre in allGenres {
            let auxGenre = (currentGenre as! NSDictionary)
            let idsArray = (dataMovie.value(forKey: "genre_ids") as! NSArray)
            let idValue: Int = (auxGenre.value(forKey: "id") as! Int)
            for dataIds in idsArray {
                if idValue == (dataIds as! Int) {
                    let newGenre = String(format: "%@,", (auxGenre.value(forKey: "name") as! String))
                    genresText = String(format: "%@ %@", genresText, newGenre)
                }
            }
        }
        DispatchQueue.main.async {
            let removedLast: String = String(genresText.dropLast())
            self.lblGenres.text = removedLast
        }
    }

}

//
//  WelcomeScreenViewController.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 11/02/22.
//

import UIKit

class WelcomeScreenViewController: UIViewController {

    @IBOutlet var lblMovies: UILabel!
    @IBOutlet var lblTV: UILabel!
    @IBOutlet var txtQuery: UITextField!
    @IBOutlet var btnSearch: UIButton!
    @IBOutlet var tblMovies: UITableView!
    @IBOutlet var btnIcon: UIButton!
    @IBOutlet var widthForTxt: NSLayoutConstraint!
    @IBOutlet var movieOptions: UIView!
    @IBOutlet var tvOptions: UIView!
    @IBOutlet var lblPlayM: UILabel!
    @IBOutlet var lblPopM: UILabel!
    @IBOutlet var lblPlayTV: UILabel!
    @IBOutlet var lblPopTV: UILabel!
    
    var moviesResult: NSArray!
    var delegateMovie: DelegateMovie!
    var type = "movie"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCustomCell()
        delegateMovie = DelegateMovie(with: self)
        let finalUrl = String(format: "%@%@/popular?api_key=%@",BASE_API_URL, self.type, API_KEY)
        delegateMovie.invokeServiceForTopTen(urlService: finalUrl)
        configureClic()
    }
    
    func configureClic() {
        let tapM = UITapGestureRecognizer(target: self, action: #selector(tapMovies))
        lblMovies.isUserInteractionEnabled = true
        lblMovies.addGestureRecognizer(tapM)
        
        let tapTV = UITapGestureRecognizer(target: self, action: #selector(tapTV))
        lblTV.isUserInteractionEnabled = true
        lblTV.addGestureRecognizer(tapTV)
        
        let tapPlayM = UITapGestureRecognizer(target: self, action: #selector(tapPlayingM))
        lblPlayM.isUserInteractionEnabled = true
        lblPlayM.addGestureRecognizer(tapPlayM)
        
        let tapPopM = UITapGestureRecognizer(target: self, action: #selector(tapPopularM))
        lblPopM.isUserInteractionEnabled = true
        lblPopM.addGestureRecognizer(tapPopM)
        
        let tapPlayTV = UITapGestureRecognizer(target: self, action: #selector(tapPlayingTV))
        lblPlayTV.isUserInteractionEnabled = true
        lblPlayTV.addGestureRecognizer(tapPlayTV)
        
        let tapPopTV = UITapGestureRecognizer(target: self, action: #selector(tapPopularTV))
        lblPopTV.isUserInteractionEnabled = true
        lblPopTV.addGestureRecognizer(tapPopTV)
    }
    
    @objc func tapTV(sender:UITapGestureRecognizer) {
        type = "tv"
        tvOptions.isHidden = !tvOptions.isHidden
        hideSearchOption()
        if movieOptions.isHidden == false {
            movieOptions.isHidden = true
        }
    }
    
    @objc func tapMovies(sender:UITapGestureRecognizer) {
        type = "movie"
        movieOptions.isHidden = !movieOptions.isHidden
        hideSearchOption()
        if tvOptions.isHidden == false {
            tvOptions.isHidden = true
        }
    }
    
    @objc func tapPlayingM(sender:UITapGestureRecognizer) {
        let finalUrl = String(format: "%@%@/now_playing?api_key=%@",BASE_API_URL, self.type, API_KEY)
        delegateMovie.invokeServiceForTopTen(urlService: finalUrl)
        movieOptions.isHidden = !movieOptions.isHidden
    }
    
    @objc func tapPopularM(sender:UITapGestureRecognizer) {
        let finalUrl = String(format: "%@%@/popular?api_key=%@",BASE_API_URL, self.type, API_KEY)
        delegateMovie.invokeServiceForTopTen(urlService: finalUrl)
        movieOptions.isHidden = !movieOptions.isHidden
    }
    
    @objc func tapPlayingTV(sender:UITapGestureRecognizer) {
        let finalUrl = String(format: "%@%@/on_the_air?api_key=%@",BASE_API_URL, self.type, API_KEY)
        delegateMovie.invokeServiceForTopTen(urlService: finalUrl)
        tvOptions.isHidden = !tvOptions.isHidden
    }
    
    @objc func tapPopularTV(sender:UITapGestureRecognizer) {
        let finalUrl = String(format: "%@%@/popular?api_key=%@",BASE_API_URL, self.type, API_KEY)
        delegateMovie.invokeServiceForTopTen(urlService: finalUrl)
        tvOptions.isHidden = !tvOptions.isHidden
    }
    
    func hideSearchOption() {
        self.btnIcon.isHidden = false
        self.btnSearch.isHidden = true
        self.widthForTxt.constant = 0
        self.txtQuery.text = ""
    }
    
    @IBAction func actionSearch(_ sender: Any) {
        if let queryText = txtQuery.text {
            type = "movie"
            self.btnIcon.isHidden = !self.btnIcon.isHidden
            self.txtQuery.text = ""
            self.widthForTxt.constant = 0
            self.btnSearch.isHidden = !self.btnSearch.isHidden
            let finalUrl = String(format: "%@search/movie?api_key=%@&query=%@&page=1", BASE_API_URL, API_KEY, queryText)
            delegateMovie.invokeServiceForSearchQuery(urlService: finalUrl)
            //search/movie?
        }
    }
    
    @IBAction func actionIcon(_ sender: Any) {
        self.btnIcon.isHidden = !self.btnIcon.isHidden
        self.widthForTxt.constant = 120
        self.btnSearch.isHidden = !self.btnSearch.isHidden
        movieOptions.isHidden = true
        tvOptions.isHidden = true
    }
    
    func receiveMoviesResult(movies: NSArray) {
        self.moviesResult = movies
        DispatchQueue.main.async {
            self.tblMovies.reloadData()
        }
    }
    
    func registerCustomCell() {
        //info
        let infoMovie = UINib(nibName: "InfoMovieCell", bundle: nil)
        let infoMovieIdentifier = "infoMovieCell"
        tblMovies.register(infoMovie, forCellReuseIdentifier: infoMovieIdentifier)
    }
    
}
//tableview
extension WelcomeScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.moviesResult != nil {
            return self.moviesResult.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let infoMovie = tableView.dequeueReusableCell(withIdentifier: "infoMovieCell") as? InfoMovieCell
        infoMovie?.isMovie = (self.type == "movie") ? true: false
        infoMovie?.setInfoCell(data: (self.moviesResult[indexPath.row] as! NSDictionary))
        return infoMovie!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tableIndex = indexPath.row
        let title = ((self.moviesResult[tableIndex] as! NSDictionary).value(forKey: (self.type == "movie") ? "title": "name") as! String)
        print("diste click en: \(title)")
        
        let detailView = DetailViewController()
        detailView.dataMovie = (self.moviesResult[indexPath.row] as! NSDictionary)
        detailView.delegateMovie = self.delegateMovie
        detailView.isMovie = (self.type == "movie") ? true: false
        self.navigationController?.pushViewController(detailView, animated: true)
    }
}


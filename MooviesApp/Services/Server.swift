//
//  Server.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 10/02/22.
//

import Foundation

class Server {
    var myDelegate: DelegateMovie!
    
    init(with delegate: DelegateMovie) {
        self.myDelegate = delegate
    }
    
    func simpleGetUrlRequestWithErrorHandling(urlService: String) {
        let url = URL(string: urlService)!
        let request = URLRequest(url: url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { [self] (data, response, error) in
            if let errorLocal = error {
                print("Error: \(errorLocal)")
            } else if let data = data {
                print("Handle HTTP request response")
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    if let results = (json as AnyObject).value(forKey: "results") {
                        myDelegate.delegateReceiveMoviesResult(movies: results as! NSArray)
                        return
                    }
                    if let genres = (json as AnyObject).value(forKey: "genres") {
                        myDelegate.delegateReceiveGenres(genres: genres as! NSArray)
                        return
                    }
                    
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            } else {
                print("Error inesperado")
            }
        }
        task.resume()
    }    
}

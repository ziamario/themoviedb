//
//  DelegateMovie.swift
//  MooviesApp
//
//  Created by Mario Alberto Vargas Rojas on 10/02/22.
//

import Foundation
import UIKit

class DelegateMovie {
    var myView: WelcomeScreenViewController!
    var myDetail: DetailViewController!
    var myServer: Server!
    
    init(with mainView: WelcomeScreenViewController) {
        self.myView = mainView
        self.myServer = Server(with: self)
    }
    
    func invokeServiceForTopTen(urlService: String) {
        myServer.simpleGetUrlRequestWithErrorHandling(urlService: urlService)
    }
    
    func invokeServiceForSearchQuery(urlService: String) {
        myServer.simpleGetUrlRequestWithErrorHandling(urlService: urlService)
    }
    
    func delegateReceiveMoviesResult(movies: NSArray) {
        self.myView.receiveMoviesResult(movies: movies)
    }
    
    func invokeGenres(type: String) {
        let url = String(format: "%@%@/list?api_key=%@", BASE_GENRES_URL, type, API_KEY)
        myServer.simpleGetUrlRequestWithErrorHandling(urlService: url)
    }
    func delegateReceiveGenres(genres: NSArray) {
        self.myDetail.receiveGenres(genres: genres)
    }
}
